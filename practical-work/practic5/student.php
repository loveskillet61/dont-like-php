<?php
class Student
{
    public $name;
    private $admisYear;
    protected $idStud;

    public function setID($idStud)
    {
        $this->idStud = $idStud;
    }

    public function getId()
    {
        return $this->idStud;    
    }

    public function __construct($name, $year, $id)
    {
            $this->name = $name; 
            $this->admisYear = $year;
            $this->idStud = $id; 

            echo 'Name: '.$name.'<br>'.'Admission year: '.$year.'<br>'.'Id Student: '.$id.'<br>';
    }

    public function leftToStudy()
    {
        $y= $this->admisYear ;
        $year = new DateTime($y);
        $period = new DateInterval('P3Y10M');
        $study = $year->add($period);
        $study = $study->format('d.m.Y');
        $toStudy = new DateTime($study);
        $t = date('d.m.Y');
        $today = new DateTime($t);

        $lts = $toStudy->diff($today);

        $leftToStudy = $lts->format('%a days');
        $toStud = $toStudy->format('d.m.Y');
        echo 'graduation '.$toStud.'<br>'.'Left to study '.$leftToStudy.'<br><br>';

    }

}

