<?php
$name = $_POST['name'];
$lastname = $_POST['lastname'];
$age = $_POST['age'];
$mail = $_POST['mail'];
$pswd = $_POST['password'];
$patpswd = "/^[0-9a-z-\.]+\@[0-9a-z-]{2,}\.[a-z]{2,5}$/i";

if(!preg_match('/[А-Яа-яЁёA-Za-z]{2,10}/', $name)){
    echo 'Имя должно состоять только из букв'.'<br>';
}

if(!preg_match('/[А-Яа-яЁёA-Za-z]{2,14}/', $lastname)) {
    echo 'Фамилия должна состоять только из букв'.'<br>';
}

if(!preg_match('/[0-9]{2}/', $age)){
    echo 'Неправильно введен возраст'.'<br>';
}

if($mail == ''){
    echo 'Введите ваш email'.'<br>';
} else if (!preg_match($patpswd, $mail)){
echo 'Неправильно введен email'.'<br>';
}

if(!preg_match('/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,14}/', $pswd)){
    echo 'Ваш пароль должен содержать хотя бы одну цифру, одну латинскую букву в нижнем и верхнем регистре. Длина от 8 до 14 символов';
}


