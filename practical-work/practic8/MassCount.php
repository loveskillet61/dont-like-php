<?php

interface MassCount
{
    public function massCount($cap); 
    public function logError($cap);
}
