<?php
require_once 'Variables.php';
require_once 'MassCount.php';


use AppleC\Variables as Variables;

class AppleCalculator extends Variables implements MassCount
{
    public function logError($capacity){
        $this->$capacity = $capacity;
        $errNum = "Вы ввели не число\n";
        $errNeg = "Вы ввели число меньше нуля\n";
        $path = './log/errorLog.txt';
        $openFile = fopen($path, 'a');
        
        if (!is_numeric($capacity)){
            fwrite($openFile, $errNum);
            fclose($openFile);
        } else if ($capacity <=0 ){
            fwrite($openFile, $errNeg);
            fclose($openFile);
        }
    }
    public function massCount($capacity)
    {
        $this->$capacity = $capacity;
        if($capacity > 0){
            $mass = $capacity * parent::BASKETS;
            echo "\n Вместимость корзин: ".$capacity."\n Масса яблок: ";
            return $mass;
        } else{
            echo "\n Ошибка, проверьте errorLog";
        }
        AppleCalculator::logError($capacity);
    }
}
