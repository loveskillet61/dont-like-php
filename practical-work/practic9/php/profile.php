<?php 
require 'db.php';
$info = $_SESSION['login_user'];
$mail = $info['email'];
$query = $connect->query("SELECT * FROM `user` WHERE `email` = '$mail'");
$array = $query->fetch_assoc();
$id = $array['id'];


$data = $_POST;
if (isset($data['update'])){
  $errors = [];
  $email = $data['email'];
  $lastname = $data['lastname'];
  $firstname = $data['firstname'];
  $age = $data['age'];
  $password = $data['password'];

  if(!preg_match('/[А-Яа-яЁёA-Za-z]{2,10}/', $firstname)){
    $errors[] = 'Имя должно состоять только из букв';
  }

  if(!preg_match('/[А-Яа-яЁёA-Za-z]{2,14}/', $lastname)) {
    $errors[] = 'Фамилия должна состоять только из букв';
  }

  if(!preg_match('/[0-9]{2}/', $age)){
    $errors[] = 'Неправильно введен возраст';
  }

  if($email == ''){
    $errors[] = 'Введите ваш email';
  } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
    $errors[] = 'Неправильно введен email';
  } else if(($query->num_rows == 1) && ($email != $mail)){
    $errors[] = 'Пользователь с такой почтой уже загеристрирован';
  }

  if(!preg_match('/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,14}/', $password)){
    $errors[] = 'Ваш пароль должен содержать хотя бы одну цифру, одну латинскую букву в нижнем и верхнем регистре. Длина от 8 до 14 символов';
  }

  if(empty($errors)){
    $connect->query("UPDATE `user` SET `email` = '$email', `firstname` = '$firstname', `lastname` = '$lastname', `age` = '$age', `password` = '$password' WHERE `id` = '$id'");
    echo "<script>alert('Данные изменены')</script>";
  } else{
    echo '<div style="color: red;">'.array_shift($errors).'</div>';
  }

}
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="../css/profile.css">
  <title>Profile</title>
</head>
<body>
  <section>
    <header>
      <h1>Profile</h1>
      <a href="./logout.php" class="exit">Выйти</a>
    </header>
    <form action="../php/profile.php" method="POST" class="data">
      <p>Имя: <input type="text" name="firstname" value="<?= $array['firstname'] ?>"></p>
      <p>Фамилия: <input type="text" name="lastname" value="<?= $array['lastname'] ?>"></p>
      <p>Пол: <?php if($array['gender'] == 'male') {echo 'Мужской';} else echo 'Женский'; ?></p> 
      <p>Возраст: <input type="text" name="age" value="<?= $array['age'] ?>"></p>
      <p>Email: <input type="email" name="email" value="<?= $array['email'] ?>"></p>
      <p>Пароль: <input type="password" name="password" value="<?= $array['password'] ?>"><mark>Показать пароль</mark></p>
      <input id="btn" type="submit" name="update" value="Редактировать профиль">
    </form>
  </section>
  <script src="../js/profile.js"></script>
</body>
</html>