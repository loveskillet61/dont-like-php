<?php
require 'db.php';

$data = $_POST;
$registr = false;
if(isset($data['signup'])){
	$errors = [];
	$firstname = $data['firstname'];
	$lastname = $data['lastname'];
	$age = $data['age'];
	$email = $data['email'];
	$password = $data['password'];
	$gender = $data['gender'];
	$num_email = $connect->query("SELECT * FROM `user` WHERE `email` = '$email'");

	if(!preg_match('/[А-Яа-яЁёA-Za-z]{2,10}/', $firstname)){
		$errors[] = 'Имя должно состоять только из букв';
	}

	if(!preg_match('/[А-Яа-яЁёA-Za-z]{2,14}/', $lastname)) {
		$errors[] = 'Фамилия должна состоять только из букв';
	}

	if(!preg_match('/[0-9]{2}/', $age)){
		$errors[] = 'Неправильно введен возраст';
	}

	if($email == ''){
		$errors[] = 'Введите ваш email';
	} else if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
		$errors[] = 'Неправильно введен email';
	} else if($num_email->num_rows == 1){
		$errors[] = 'Пользователь с такой почтой уже загеристрирован';
	}

	if(!preg_match('/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,14}/', $password)){
		$errors[] = 'Ваш пароль должен содержать хотя бы одну цифру, одну латинскую букву в нижнем и верхнем регистре. Длина от 8 до 14 символов';
	}

	if(empty($errors)){
		$connect->query("INSERT INTO `user`(`firstname`, `lastname`, `email`, `password`, `age`, `gender`) VALUES ('$firstname', '$lastname', '$email', '$password', '$age', '$gender')");
		echo "<script>alert('Вы успешно загеристрированы')</script>";
		$registr = true;
	} else{
		echo '<div style="color: red;">'.array_shift($errors).'</div>';
	}
}
?> 

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="../css/style.css">
    <title>Registration</title>
</head>
<body>
	<section class="form">
		<div>
			<h1>Регистрация</h1>
			<form action="../php/signup.php" method="POST">
				<input type="text" name="firstname" placeholder="Имя" required value="<?php if(!$registr){ echo @$data['firstname'];}?>">
				<input type="text" name="lastname" placeholder="Фамилия" required value="<?php if(!$registr){echo @$data['lastname'];}?>">
				<label>Ваш пол:</label>
				<fildset class="radiobutton">
					<label for="male">Мужской</label><input type="radio" name="gender" id="male" value="male" checked>
					<label for="female">Женский</label><input type="radio" name="gender" id="female" value="female">
				</fildset>
				<input type="text" name="age" placeholder="Возраст" value="<?php if(!$registr){echo @$data['age'];}?>">
				<input type="email" name="email" placeholder="Ваш email" value="<?php if(!$registr){echo @$data['email'];}?>">
				<input type="password" name="password" placeholder="Пароль" autocomplete="off" required>
				<input type="submit" name="signup" value="Зарегистрироваться">
				<a href="./login.php">Авторизироваться</a>
			</form>
		</div>
	</section>
</body>
</html>