<?php
//вынести класс в пространство имен, методы в интерфейс ,поля в абстрактный класс
require_once 'Variables.php';
require_once 'MassCount.php';


use AppleC\Variables as Variables;

class AppleCalculator extends Variables implements MassCount
{
    public function massCount($capacity)
    {
        $this->$capacity = $capacity;
        $mass = $capacity * parent::BASKETS;
        echo 'Вместимость корзин: '.$capacity.'<br> Масса яблок: ';
        return $mass;
    }
}
