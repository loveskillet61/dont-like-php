Задание: <br>
Напишите функцию, которая заменяет все цифры на английское название
этой цифры в строке и возвращает новую строку.<br>

<?php
$str = '012f9r';

function replace_numb($string){
    $numb = ['zero ', 'one ', 'two ', 'three ', 'four ', 'five ', 'six ', 'seven ', 'eight ', 'nine '];
    $number = str_split($string);
    $new_str = '';
    foreach ($number as $key => $value){
        if(array_key_exists($value, $numb)){
            $new_str.= $numb[$value];
        } else {
            $new_str.= 'NaN ';
        }
    }
    return $new_str;
}
echo $str.'<br>';
echo replace_numb($str);
